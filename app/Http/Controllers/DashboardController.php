<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use \Auth;


class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $text  = 'O usuário está deslogado';
        if (Auth::check()) {
              $text =   'O usuário está logado...';
         }

        //die($text);
        return view('admin.index');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
