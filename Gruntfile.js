module.exports = function(grunt) {

  //Initializing the configuration object
    grunt.initConfig({

      // Task configuration
    less: {
        development: {
            options: {
              compress: true,  //minifying the result
            },
            files: {
              //compiling frontend.less into frontend.css
              "./public/assets/css/frontend.css":"./public/assets/css/frontend.less",
              //compiling backend.less into backend.css
              "./public/assets/css/backend.css":"./public/assets/css/backend.less"
            }
        }
    },

    concat: {
        options: {
          separator: ';',
        },
        javascript: {
          src: ['./vendor/components/jquery/jquery.min.js','./vendor/twitter/bootstrap/dist/js/bootstrap.min.js','./public/assets/javascript/frontend.js'],
          dest: './public/assets/javascript/frontend.js',
        },
      },

    uglify: {
      options: {
        mangle: false  // Use if you want the names of your functions and variables unchanged
      },
      frontend: {
        files: {
          './public/assets/javascript/frontend.js': './public/assets/javascript/frontend.js',
        }
      },
      backend: {
        files: {
          './public/assets/javascript/backend.js': './public/assets/javascript/backend.js',
        }
      },
    },


    watch: {
        js: {
          files: ['./public/assets/javascript/*.*'],   //arquivos monitorados
          tasks: ['concat:javascript','uglify'],     //tarefas executadas
          options: {
            livereload: true                        //atualiza o navegador
          }
        },
        less: {
          files: ['./public/assets/css/*.*'],  //arquivos monitorados
          tasks: ['less'],                          //tarefas executadas
          options: {
            livereload: true                        //atualiza o navegador
          }
        }
      }


    });

  // Plugin loading
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-phpunit');
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Task definition
  grunt.registerTask('init', ['copy', 'less', 'concat', 'uglify']);
  grunt.registerTask('default', ['watch']);

};