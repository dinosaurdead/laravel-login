# Sistema De login (Básico)

Sistema básico de login criado com a classe Auth do Laravel.
Possui uma tela para cadastrar novos usuários, tela de login com opção para manter logado e sistema de bloqueio, para determinadas páginas, caso não tenha ninguém logado. 

## Instalando o projeto

Primeiro é necessário clonar:

```
$ git clone https://dinosaurdead@bitbucket.org/dinosaurdead/laravel-login.git/wiki
```
Depois de clonado entre na pasta do projeto e baixe as dependências

```
cd laravel-login

php composer.phar install
```

## Configurando os dados de acesso

Primeiro tem de criar o arquivo .env, na raiz do projeto, e inserir as configurações do seu projeto.
exemplo de arquivo .env:
```
APP_ENV=local
APP_DEBUG=true
APP_KEY=SomeRandomString
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=studies
DB_USERNAME=root
DB_PASSWORD=root

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
```
Agora é necessário gerar um app_key.

na raiz do projeto digite:
```
php artisan key:generate
```

## Rodando o projeto

na raiz do projeto digite:
```
php artisan serve
```
Ao rodar o código acima irá subir um server na porta que estiver disponível,portanto pode ser em uma porta diferente da utilizada na documentação.

##URLS

Página padrão do laravel
```
http://localhost:8000/
```

Login
```
http://localhost:8000/login
```

Cadastrar usuários
```
http://localhost:8000/register
```

Página que só permite o acesso logado e que contém o botão para deslogar
```
http://localhost:8000/dashboard
```